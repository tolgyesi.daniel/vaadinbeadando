package hu.pte.ttk.vaadin.beadando.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import hu.pte.ttk.vaadin.beadando.entity.Role;
import hu.pte.ttk.vaadin.beadando.entity.User;
import hu.pte.ttk.vaadin.beadando.service.RoleService;
import hu.pte.ttk.vaadin.beadando.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Route(value = "/user", layout = MainLayout.class)
@PageTitle("Users")
public class ListView extends VerticalLayout {

    private User user;

    @Autowired
    private UserService service;
    @Autowired
    private RoleService roleService;

    private VerticalLayout form;
    private Binder<User> binder;
    private Button deleteBtn =  new Button("Delete", VaadinIcon.TRASH.create());

    private TextField userFirstName;
    private TextField userLastName;
    private TextField userName;
    private ComboBox<Role> userRole;

    private HorizontalLayout container = new HorizontalLayout();
    private Span passwordNotify = new Span();

    @PostConstruct
    public void init(){
        add(new H3("Users"));
        Grid<User> grid = new Grid<>();
        grid.setItems(service.getAll());
        grid.addColumn(User::getUserFirstName).setHeader("First name");
        grid.addColumn(User::getUserLastName).setHeader("Last Name");
        grid.addColumn(User::getUserName).setHeader("User name");
        grid.addColumn(userEntity -> {
            if (userEntity.getAuthorities() != null){
                StringBuilder builder = new StringBuilder();
                userEntity.getAuthorities().forEach(roleEntity -> {
                    builder.append(roleEntity.getAuthority()).append(",");
                });
                return builder.toString();
            }
            return "";
        }).setHeader("Role");
        grid.asSingleSelect().addValueChangeListener(event -> {
            user = event.getValue();
            binder.setBean(user);
            deleteBtn.setEnabled(user != null);
            form.setVisible(user != null);
            form.remove(passwordNotify);
        });
        grid.setWidth("70%");
        addButtonBar(grid);

        container.setWidth("100%");
        container.add(grid);
        addForm(grid);
        form.setVisible(false);

        add(container);
    }

    private void addForm(Grid<User> grid){
        form = new VerticalLayout();
        binder = new Binder<>(User.class);

        HorizontalLayout firstNameField = new HorizontalLayout();
        userFirstName = new TextField();
        userFirstName.setLabel("First Name");
        firstNameField.add(userFirstName);
        firstNameField.setWidth("100%");
        userFirstName.setWidth("70%");

        HorizontalLayout lastNameField = new HorizontalLayout();
        userLastName = new TextField();
        userLastName.setLabel("Last Name");
        lastNameField.add(userLastName);
        lastNameField.setWidth("100%");
        userLastName.setWidth("70%");

        HorizontalLayout userNameField = new HorizontalLayout();
        userName = new TextField();
        userName.setLabel("User Name");
        userNameField.add(userName);
        userNameField.setWidth("100%");
        userName.setWidth("70%");

        HorizontalLayout roleField = new HorizontalLayout();
        userRole = new ComboBox<>();
        userRole.setItems(roleService.getAll());
        userRole.setItemLabelGenerator(Role::getAuthority);
        userRole.setLabel("User Role");
        roleField.add(userRole);
        roleField.setWidth("100%");
        userRole.setWidth("70%");

        form.add(firstNameField, lastNameField, userNameField, roleField, addSaveBtn(grid));
        form.setWidth("30%");
        form.setHorizontalComponentAlignment(Alignment.CENTER);
        container.add(form);
        binder.bindInstanceFields(this);
        form.remove(passwordNotify);
    }

    private Button addSaveBtn(Grid<User> grid){
        Button saveBtn = new Button("Save", VaadinIcon.PLUS.create());
        saveBtn.addClickListener(buttonClickEvent -> {
            if (user.getId() == null){
                User u = new User();
                String generatedUserPassword = passwordGenerator();
                int strength = 10;
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());

                u.setUserFirstName(user.getUserFirstName());
                u.setUserLastName(user.getUserLastName());
                u.setUserName(user.getUserName());
                u.setAuthorities(Collections.singletonList(userRole.getValue()));
                u.setUserPassword(bCryptPasswordEncoder.encode(generatedUserPassword));

                service.add(u);
                grid.setItems(service.getAll());
                Notification.show("Successful save");

                passwordNotify = new Span("User generated password: "+generatedUserPassword);
                passwordNotify.setVisible(true);
                form.add(passwordNotify);
                form.setVisible(true);
            }
            else{
                List<Role> roles = new ArrayList<>();
                roles.add(userRole.getValue());
                user.setUserFirstName(userFirstName.getValue());
                user.setUserLastName(userLastName.getValue());
                user.setAuthorities(roles);

                service.update(user);
                grid.setItems(service.getAll());
                form.remove(passwordNotify);
                Notification.show("Successful modify");
            }
        });
        return saveBtn;
    }

    private void addButtonBar(Grid<User> grid){
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        deleteBtn.addClickListener(buttonClickEvent -> {
            service.remove(user);
            Notification.show("Successful delete");
            user = null;
            grid.setItems(service.getAll());
            form.setVisible(true);
            form.remove(passwordNotify);
        });
        deleteBtn.setEnabled(false);

        Button addBtn = new Button("Add", VaadinIcon.PLUS.create());
        addBtn.addClickListener(buttonClickEvent -> {
            user = new User();
            binder.setBean(user);
            form.setVisible(true);
            form.remove(passwordNotify);
        });
        horizontalLayout.add(deleteBtn);
        horizontalLayout.add(addBtn);
        add(horizontalLayout);
    }

    private String passwordGenerator(){
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        int length = 6;
        for (int i = 0; i < length; i++){
            int index = random.nextInt(alphabet.length());
            char randomChar = alphabet.charAt(index);
            sb.append(randomChar);
        }

        return sb.toString();
    }

}
