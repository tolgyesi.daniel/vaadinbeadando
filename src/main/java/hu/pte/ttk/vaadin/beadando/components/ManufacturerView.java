package hu.pte.ttk.vaadin.beadando.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import hu.pte.ttk.vaadin.beadando.entity.Manufacturer;
import hu.pte.ttk.vaadin.beadando.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Route(value = "/manufacturer", layout = MainLayout.class)
@PageTitle("Manufacturer")
public class ManufacturerView extends VerticalLayout {

    @Autowired
    private ManufacturerService service;

    private Manufacturer manufacturer;
    private final static String FIELDNAME= "manufacturer_name";

    private HorizontalLayout container = new HorizontalLayout();
    private VerticalLayout form;
    private Binder<Manufacturer> binder;
    private Button deleteBtn =  new Button("Delete", VaadinIcon.TRASH.create());
    private TextField manufacturerName;
    private TextField filter = new TextField();
    private Grid<Manufacturer> grid = new Grid<>();

    @PostConstruct
    public void init(){
        add(new H3("Manufacturer"));
        grid.setItems(service.findByName(filter.getValue(), FIELDNAME));
        Grid.Column<Manufacturer> mNameColumn = grid.addColumn(Manufacturer::getManufacturerName).setHeader("Manufacturer name");
        GridSortOrder<Manufacturer> order = new GridSortOrder<>(mNameColumn, SortDirection.DESCENDING);
        grid.sort(Arrays.asList(order));

        grid.asSingleSelect().addValueChangeListener(event -> {
            manufacturer = event.getValue();
            binder.setBean(manufacturer);
            deleteBtn.setEnabled(manufacturer != null);
            form.setVisible(manufacturer != null);
        });
        grid.setWidth("70%");
        addButtonBar(grid);

        filter.setPlaceholder("Filter");
        filter.setClearButtonVisible(true);
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(event ->
            refreshGrid(filter.getValue())
        );

        container.setWidth("100%");
        container.add(grid);
        addForm(grid);
        form.setVisible(false);

        add(container);
    }

    private void addButtonBar(Grid<Manufacturer> grid){
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        deleteBtn.addClickListener(buttonClickEvent -> {
            service.remove(manufacturer);
            Notification.show("Successful delete");
            manufacturer = null;
            grid.setItems(service.getAll());
            form.setVisible(true);
        });
        deleteBtn.setEnabled(false);

        Button addBtn = new Button("Add", VaadinIcon.PLUS.create());
        addBtn.addClickListener(buttonClickEvent -> {
            manufacturer = new Manufacturer();
            binder.setBean(manufacturer);
            form.setVisible(true);
        });
        horizontalLayout.add(deleteBtn);
        horizontalLayout.add(addBtn);
        horizontalLayout.add(filter);
        add(horizontalLayout);
    }

    private void addForm(Grid<Manufacturer> grid){
        form = new VerticalLayout();
        binder = new Binder<>(Manufacturer.class);

        HorizontalLayout manufacturerNameField = new HorizontalLayout();
        manufacturerName = new TextField();
        manufacturerName.setLabel("Manufacturer name");
        manufacturerNameField.add(manufacturerName);
        manufacturerNameField.setWidth("100%");
        manufacturerName.setWidth("70%");
        manufacturerName.setRequired(true);

        form.add(manufacturerName, addSaveBtn(grid));
        form.setWidth("30%");
        form.setHorizontalComponentAlignment(Alignment.CENTER);
        container.add(form);
        binder.bindInstanceFields(this);
    }

    private Button addSaveBtn(Grid<Manufacturer> grid){
        Button saveBtn = new Button("Save", VaadinIcon.PLUS.create());
        saveBtn.addClickListener(buttonClickEvent -> {
            if (manufacturer.getId() == null){
                Manufacturer m = new Manufacturer();
                m.setManufacturerName(manufacturer.getManufacturerName());
                service.add(m);
                grid.setItems(service.getAll());
                Notification.show("Successful save");
                form.setVisible(true);
            }
            else{
                manufacturer.setManufacturerName(manufacturerName.getValue());
                service.update(manufacturer);
                grid.setItems(service.getAll());
                Notification.show("Successful modify");
            }
        });
        return saveBtn;
    }

    private void refreshGrid(String value){
        grid.setItems(service.findByName(value, FIELDNAME));
    }

}
