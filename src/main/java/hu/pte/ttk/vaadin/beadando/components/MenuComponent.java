package hu.pte.ttk.vaadin.beadando.components;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import hu.pte.ttk.vaadin.beadando.security.SecurityUtils;

public class MenuComponent extends VerticalLayout {

    public MenuComponent() {
        Anchor main = new Anchor();
        main.setText("Dashboard");
        main.setHref("/");
        add(main);

        Anchor vehicle = new Anchor();
        vehicle.setText("Vehicle");
        vehicle.setHref("/vehicle");
        add(vehicle);

        Anchor manufacturer = new Anchor();
        manufacturer.setText("Manufacturer");
        manufacturer.setHref("/manufacturer");
        add(manufacturer);

        if(SecurityUtils.isAdmin()){
            Anchor user = new Anchor();
            user.setText("Users page");
            user.setHref("/user");
            add(user);
        }
    }

}
