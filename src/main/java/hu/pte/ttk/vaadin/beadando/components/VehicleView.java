package hu.pte.ttk.vaadin.beadando.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import hu.pte.ttk.vaadin.beadando.entity.Manufacturer;
import hu.pte.ttk.vaadin.beadando.entity.Vehicle;
import hu.pte.ttk.vaadin.beadando.service.ManufacturerService;
import hu.pte.ttk.vaadin.beadando.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Route(value = "/vehicle", layout = MainLayout.class)
@PageTitle("Vehicle")
public class VehicleView extends VerticalLayout {

    @Autowired
    private VehicleService service;

    @Autowired
    private ManufacturerService manufacturerService;

    private Vehicle vehicle;
    private final static String FIELDNAME = "vehicle_name";
    private final static String FIELDNAMESECOND = "vehicle_create_date";

    private HorizontalLayout container = new HorizontalLayout();
    private VerticalLayout form;
    private Binder<Vehicle> binder;
    private Button deleteBtn = new Button("Delete", VaadinIcon.TRASH.create());
    private TextField vehicleNameFilter = new TextField();
    private TextField vehicleCreateDateFilter = new TextField();
    private Grid<Vehicle> grid = new Grid<>();

    private TextField vehicleName;
    private TextField vehicleType;
    private ComboBox<Manufacturer> vehicleMaker;
    private IntegerField vehicleDoors;
    private IntegerField vehicleCreateDate;

    @PostConstruct
    public void init(){
        add(new H3("Vehicles"));
        grid.setItems(service.getAll());
        Grid.Column<Vehicle> nNameColumn = grid.addColumn(Vehicle::getVehicleName).setHeader("Vehicle Name");
        Grid.Column<Vehicle> nTypeColumn = grid.addColumn(Vehicle::getVehicleType).setHeader("Vehicle Type");
        Grid.Column<Vehicle> nMakerColumn = grid.addColumn( vehicleEntity -> {
            if (vehicleEntity.getVehicleMaker() != null){
                return manufacturerService.findById(vehicleEntity.getVehicleMaker().getId()).getManufacturerName();
            }
            return "";
        }).setHeader("Vehicle Maker");
        Grid.Column<Vehicle> nDoorsColumn = grid.addColumn(Vehicle::getVehicleDoors).setHeader("Vehicle Doors");
        Grid.Column<Vehicle> nCreatedColumn = grid.addColumn(Vehicle::getVehicleCreateDate).setHeader("Vehicle Create Date");
        GridSortOrder<Vehicle> orderName = new GridSortOrder<>(nNameColumn, SortDirection.DESCENDING);
        GridSortOrder<Vehicle> orderType = new GridSortOrder<>(nTypeColumn, SortDirection.DESCENDING);
        GridSortOrder<Vehicle> orderMaker = new GridSortOrder<>(nMakerColumn, SortDirection.DESCENDING);
        GridSortOrder<Vehicle> orderDoors = new GridSortOrder<>(nDoorsColumn, SortDirection.DESCENDING);
        GridSortOrder<Vehicle> orderCreated = new GridSortOrder<>(nCreatedColumn, SortDirection.DESCENDING);
        grid.sort(Arrays.asList(orderName, orderType, orderMaker, orderDoors, orderCreated));

        grid.asSingleSelect().addValueChangeListener(event -> {
            vehicle = event.getValue();
            binder.setBean(vehicle);
            deleteBtn.setEnabled(vehicle != null);
            form.setVisible(vehicle != null);
        });

        grid.setWidth("70%");
        addButtonBar(grid);

        vehicleNameFilter.setPlaceholder("Vehicle name");
        vehicleCreateDateFilter.setPlaceholder("Vehicle Create Date");
        vehicleNameFilter.setClearButtonVisible(true);
        vehicleCreateDateFilter.setClearButtonVisible(true);

        vehicleNameFilter.setValueChangeMode(ValueChangeMode.EAGER);
        vehicleCreateDateFilter.setValueChangeMode(ValueChangeMode.EAGER);

        vehicleNameFilter.addValueChangeListener(event -> refreshGridString(vehicleNameFilter.getValue()));
        vehicleCreateDateFilter.addValueChangeListener(event -> refreshGridInteger(vehicleCreateDateFilter.getValue()));

        container.setWidth("100%");
        container.add(grid);
        addForm(grid);
        form.setVisible(false);

        add(container);

    }

    private void addButtonBar(Grid<Vehicle> grid){
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        deleteBtn.addClickListener(buttonClickEvent -> {
            service.remove(vehicle);
            Notification.show("Successful delete");
            vehicle = null;
            grid.setItems(service.getAll());
            form.setVisible(true);
        });
        deleteBtn.setEnabled(false);

        Button addBtn = new Button("Add", VaadinIcon.PLUS.create());
        addBtn.addClickListener(buttonClickEvent -> {
            vehicle = new Vehicle();
            binder.setBean(vehicle);
            form.setVisible(true);
        });
        horizontalLayout.add(deleteBtn);
        horizontalLayout.add(addBtn);
        horizontalLayout.add(vehicleNameFilter, vehicleCreateDateFilter);
        add(horizontalLayout);
    }

    private void addForm(Grid<Vehicle> grid){
        form = new VerticalLayout();
        binder = new Binder<>(Vehicle.class);

        HorizontalLayout vehicleNameFieldContainer = new HorizontalLayout();
        vehicleName = new TextField();
        vehicleName.setLabel("Vehicle name");
        vehicleNameFieldContainer.add(vehicleName);
        vehicleNameFieldContainer.setWidth("100%");
        vehicleName.setWidth("70%");
        vehicleName.setRequired(true);

        HorizontalLayout vehicleTypeFieldContainer = new HorizontalLayout();
        vehicleType = new TextField();
        vehicleType.setLabel("Vehicle type");
        vehicleTypeFieldContainer.add(vehicleType);
        vehicleTypeFieldContainer.setWidth("100%");
        vehicleType.setWidth("70%");
        vehicleType.setRequired(true);

        HorizontalLayout vehicleMakerFieldContainer = new HorizontalLayout();
        vehicleMaker = new ComboBox<>();
        vehicleMaker.setItems(manufacturerService.getAll());
        vehicleMaker.setItemLabelGenerator(Manufacturer::getManufacturerName);
        vehicleMaker.setLabel("Vehicle manufacturer");
        vehicleMakerFieldContainer.add(vehicleMaker);
        vehicleMakerFieldContainer.setWidth("100%");
        vehicleMaker.setWidth("70%");
        vehicleMaker.setRequired(true);

        HorizontalLayout vehicleDoorsFieldContainer = new HorizontalLayout();
        vehicleDoors = new IntegerField();
        vehicleDoors.setLabel("Vehicle doors");
        vehicleDoorsFieldContainer.add(vehicleDoors);
        vehicleDoorsFieldContainer.setWidth("100%");
        vehicleDoors.setWidth("70%");
        vehicleDoors.setRequiredIndicatorVisible(true);

        HorizontalLayout vehicleCreateDateFieldContainer = new HorizontalLayout();
        vehicleCreateDate = new IntegerField();
        vehicleCreateDate.setLabel("Vehicle create date");
        vehicleCreateDateFieldContainer.add(vehicleCreateDate);
        vehicleCreateDateFieldContainer.setWidth("100%");
        vehicleCreateDate.setWidth("70%");
        vehicleCreateDate.setRequiredIndicatorVisible(true);

        form.add(vehicleNameFieldContainer, vehicleTypeFieldContainer, vehicleTypeFieldContainer, vehicleMakerFieldContainer, vehicleDoorsFieldContainer, vehicleCreateDateFieldContainer, addSaveBtn(grid));
        form.setWidth("30%");
        form.setHorizontalComponentAlignment(Alignment.CENTER);
        container.add(form);
        binder.bindInstanceFields(this);
    }

    private Button addSaveBtn(Grid<Vehicle> grid){
        Button saveBtn = new Button("Save", VaadinIcon.PLUS.create());
        saveBtn.addClickListener(buttonClickEvent -> {
            if (vehicle.getId() == null){
                Vehicle m = new Vehicle();
                m.setVehicleName(vehicle.getVehicleName());
                m.setVehicleType(vehicle.getVehicleType());
                m.setVehicleMaker(vehicle.getVehicleMaker());
                m.setVehicleDoors(vehicle.getVehicleDoors());
                m.setVehicleCreateDate(vehicle.getVehicleCreateDate());
                service.add(m);
                grid.setItems(service.getAll());
                Notification.show("Successful save");
                form.setVisible(true);
            }
            else{
                vehicle.setVehicleName(vehicleNameFilter.getValue());
                service.update(vehicle);
                grid.setItems(service.getAll());
                Notification.show("Successful modify");
            }
        });
        return saveBtn;
    }

    private void refreshGridString(String value){
        grid.setItems(service.findByName(value, VehicleView.FIELDNAME));
    }

    private void refreshGridInteger(String value){
        if (!value.equals("")) {
            Integer changeValue = Integer.parseInt(value);
            grid.setItems(service.findByNumber(changeValue, VehicleView.FIELDNAMESECOND));
        }
        else{
            grid.setItems(service.getAll());
        }
    }

}
