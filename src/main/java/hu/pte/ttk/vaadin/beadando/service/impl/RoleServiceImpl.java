package hu.pte.ttk.vaadin.beadando.service.impl;

import hu.pte.ttk.vaadin.beadando.entity.Role;
import hu.pte.ttk.vaadin.beadando.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends CoreCRUDServiceImpl<Role> implements RoleService {

    @Override
    protected void updateCore(Role persistedEntity, Role entity) {
        persistedEntity.setAuthority(entity.getAuthority());
    }

    @Override
    protected Class<Role> getManagedClass() {
        return Role.class;
    }
}
