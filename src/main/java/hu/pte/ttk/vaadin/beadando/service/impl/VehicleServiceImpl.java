package hu.pte.ttk.vaadin.beadando.service.impl;

import hu.pte.ttk.vaadin.beadando.entity.Vehicle;
import hu.pte.ttk.vaadin.beadando.service.VehicleService;
import org.springframework.stereotype.Service;

@Service
public class VehicleServiceImpl extends CoreCRUDServiceImpl<Vehicle> implements VehicleService {

    @Override
    protected void updateCore(Vehicle persistedEntity, Vehicle entity){
        persistedEntity.setVehicleName(entity.getVehicleName());
        persistedEntity.setVehicleType(entity.getVehicleType());
        persistedEntity.setVehicleCreateDate(entity.getVehicleCreateDate());
        persistedEntity.setVehicleDoors(entity.getVehicleDoors());
        persistedEntity.setVehicleMaker(entity.getVehicleMaker());
    }

    @Override
    protected Class<Vehicle> getManagedClass() { return Vehicle.class; }

}
