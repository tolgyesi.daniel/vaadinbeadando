package hu.pte.ttk.vaadin.beadando.service.impl;

import hu.pte.ttk.vaadin.beadando.entity.Manufacturer;
import hu.pte.ttk.vaadin.beadando.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
public class ManufacturerServiceImpl extends CoreCRUDServiceImpl<Manufacturer> implements ManufacturerService {

    @Autowired
    private EntityManager entityManager;

    @Override
    protected void updateCore(Manufacturer persistedEntity, Manufacturer entity){
        persistedEntity.setManufacturerName(entity.getManufacturerName());
    }

    @Override
    protected Class<Manufacturer> getManagedClass() { return Manufacturer.class; }

}
