package hu.pte.ttk.vaadin.beadando.entity;

import javax.persistence.*;

@Table(name = "vehicle")
@Entity
public class Vehicle extends CoreEntity{
    @Column(name = "vehicle_name")
    private String vehicleName;

    @Column(name = "vehicle_type")
    private String vehicleType;

    @ManyToOne(fetch = FetchType.EAGER)
    private Manufacturer vehicleMaker;

    @Column(name = "vehicle_doors")
    private Integer vehicleDoors;

    @Column(name = "vehicle_create_date")
    private Integer vehicleCreateDate;

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Manufacturer getVehicleMaker() {
        return vehicleMaker;
    }

    public void setVehicleMaker(Manufacturer vehicleMaker) {
        this.vehicleMaker = vehicleMaker;
    }

    public Integer getVehicleDoors() {
        return vehicleDoors;
    }

    public void setVehicleDoors(Integer vehicleDoors) {
        this.vehicleDoors = vehicleDoors;
    }

    public Integer getVehicleCreateDate() {
        return vehicleCreateDate;
    }

    public void setVehicleCreateDate(Integer vehicleCreateDate) {
        this.vehicleCreateDate = vehicleCreateDate;
    }
}
