package hu.pte.ttk.vaadin.beadando.service;

import hu.pte.ttk.vaadin.beadando.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends CoreCRUDService<User>, UserDetailsService {

}
