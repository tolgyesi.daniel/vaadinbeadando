package hu.pte.ttk.vaadin.beadando.service;

import hu.pte.ttk.vaadin.beadando.entity.Vehicle;

public interface VehicleService extends CoreCRUDService<Vehicle> {
}
