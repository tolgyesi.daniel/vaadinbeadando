package hu.pte.ttk.vaadin.beadando.service;

import hu.pte.ttk.vaadin.beadando.entity.Role;

public interface RoleService extends CoreCRUDService<Role> {

}
