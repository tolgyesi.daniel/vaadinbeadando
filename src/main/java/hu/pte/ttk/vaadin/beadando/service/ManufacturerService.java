package hu.pte.ttk.vaadin.beadando.service;

import hu.pte.ttk.vaadin.beadando.entity.Manufacturer;

public interface ManufacturerService extends CoreCRUDService<Manufacturer> {
}
