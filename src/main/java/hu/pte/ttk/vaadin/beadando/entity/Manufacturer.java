package hu.pte.ttk.vaadin.beadando.entity;

import javax.persistence.*;

@Table(name = "manufacturer")
@Entity
public class Manufacturer extends CoreEntity{
    @Column(name = "manufacturer_name", unique = true)
    private String manufacturerName;

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
}
